#!/bin/bash

# copy stdout and stderror to log
exec 1> >(tee >(logger -t "script-out")) 2> >(tee >&2 >(logger -t "script-err"))

F_print_green() {
    echo ""
    echo -e "\e[0;32m    $1 \e[0m"
    echo ""
}

clear
F_print_green "# set params"
read -e -p "Set PostgreSQL version: " -i "1c-15" VER
read -e -p "Set password for user postgres: " POSTGRESPASS
read -e -p "Set password for user root:     " -i $POSTGRESPASS ROOTPASS
read -e -p "Set CPU core for PostgreSQL: " -i $(nproc) CPU
read -e -p "Set RAM size for PostgreSQL: " -i $(LC_MESSAGES=C free -m|awk '/Mem:/ {print $2;}') RAM
FILE="pgpro-repo-add.sh"

read -e -p "All ready. Continue?  " -i "y"
if [ "$REPLY" != "y" ]; then echo "Cancel."; exit 1; fi

F_print_green "# add pgpro repos"
curl -sSLo $FILE https://repo.postgrespro.ru/$VER/keys/$FILE && \
chmod 700 $FILE && \
./$FILE && \
echo "install postrgesql"
apt -y install postgrespro-$VER
[[ -f $FILE ]] && rm $FILE

#sed -i '/^local[ ]+all/s/peer/trust/' /var/lib/pgpro/$VER/data/pg_hba.conf && \
#service postgrespro-$VER restart

F_print_green "# create postgresql roles and set passwords"
sudo -u postgres psql -c "CREATE ROLE root WITH LOGIN SUPERUSER PASSWORD '$ROOTPASS'; ALTER USER postgres PASSWORD '$POSTGRESPASS';"
/opt/pgpro/$VER/bin/pg-wrapper links update
systemctl stop postgrespro-$VER.service

F_print_green "# replace nproc and free"
cp /usr/bin/nproc /usr/bin/nproc_orig
cp /usr/bin/free /usr/bin/free_orig
echo "echo \"$CPU\"" > /usr/bin/nproc
echo "echo \"Mem: $RAM\"" > /usr/bin/free

F_print_green "# tune 1C"
cp /var/lib/pgpro/$VER/data/postgresql.conf /var/lib/pgpro/$VER/data/postgresql.conf_bak_$(date +"%d.%m.%y")
(/opt/pgpro/$VER/share/1c.tune) > /var/lib/pgpro/$VER/data/postgresql.conf

F_print_green "# restore orig utils"
mv /usr/bin/nproc_orig /usr/bin/nproc
mv /usr/bin/free_orig /usr/bin/free

F_print_green "# start postgresql"
systemctl start postgrespro-$VER.service
systemctl status postgrespro-$VER.service

unset VER POSTGRESPASS ROOTPASS CPU RAM FILE

F_print_green "All done."

exit 0
