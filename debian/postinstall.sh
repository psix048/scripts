#!/bin/bash

# copy stdout and stderror to log
exec 1> >(tee >(logger -t "script-out")) 2> >(tee >&2 >(logger -t "script-err"))

# check
if [ -z $SUDO_USER ] || [ $(id -u) -ne 0 ]; then
    echo "Error: Run this script with sudo." >&2
    exit 1
fi

F_print_green() {
    echo ""
    echo -e "\e[0;32m    $1 \e[0m"
    echo ""
}

F_url_install() {
    for URL in $*; do
        if [[ $URL = *http* ]]; then
            F_print_green "# install ${URL##*/}"
            FILE=$(mktemp --suffix=.deb)
            wget -O $FILE $URL && \
            apt -y install $FILE
            rm $FILE
        fi
    done
}

clear

read -e -p "New hostname: " -i "$(hostname)" NEWHOSTNAME
read -e -p "New locale: " -i "ru_RU.UTF-8" NEWLOCALE

F_print_green "# All ready. Let's do it."

F_print_green "# set hostname"
hostnamectl set-hostname $NEWHOSTNAME

F_print_green "# configure locales"
update-locale "LANG=$NEWLOCALE"
locale-gen --purge "$NEWLOCALE"
dpkg-reconfigure --frontend noninteractive locales

F_print_green "# change repos"
echo "\
deb http://mirror.yandex.ru/debian "$(lsb_release -c -s)" main contrib non-free
deb-src http://mirror.yandex.ru/debian "$(lsb_release -c -s)" main contrib non-free
deb http://mirror.yandex.ru/debian "$(lsb_release -c -s)"-updates main contrib non-free
deb-src http://mirror.yandex.ru/debian "$(lsb_release -c -s)"-updates main contrib non-free
deb http://security.debian.org/debian-security/ "$(lsb_release -c -s)"-security main contrib non-free
deb-src http://security.debian.org/debian-security/ "$(lsb_release -c -s)"-security main contrib non-free
" > /etc/apt/sources.list 

F_print_green "# upgrade all"
apt -y clean && apt -y update && apt -y upgrade

F_print_green "# install some packages from repo"
apt -y install \
mc \
curl \
atop \
htop \
ncdu \
unixodbc \
gnupg2 \
cifs-utils \
imagemagick \
libgsf-1-114 \
ttf-mscorefonts-installer

F_print_green "# install some packages from web"
# call function for install directly frow url
F_url_install "http://ftp.ru.debian.org/debian/pool/main/e/enchant/libenchant1c2a_1.6.0-11.1+b1_amd64.deb \
http://ppa.launchpad.net/linuxuprising/libpng12/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1+1~ppa0~focal_amd64.deb"

F_print_green "# terminal cosmetics"
cp ~/.bashrc ~/.bashrc_bak
cp /home/$SUDO_USER/.bashrc ~/.bashrc
sed -i -e 's/\\h/\\H/g' ~/.bashrc
sed -i '/#alias ll/s/^#//g' .bashrc
sed -i '/#alias la/s/^#//g' .bashrc
source ~/.bashrc

F_print_green "# allow root ssh with key"
[ -d /root/.ssh ] || mkdir ~/.ssh
cat /home/$SUDO_USER/.ssh/authorized_keys > /root/.ssh/authorized_keys
sed -i '/#PermitRootLogin/s/^#//g' /etc/ssh/sshd_config

unset NEWHOSTNAME NEWLOCALE URL FILE

F_print_green "All done."
exit 0
