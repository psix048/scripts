#!/bin/bash

# copy stdout and stderror to log
exec 1> >(tee >(logger -t "script-out")) 2> >(tee >&2 >(logger -t "script-err"))

F_print_green() {
    echo ""
    echo -e "\e[0;32m    $1 \e[0m"
    echo ""
}

F_uninstall() {
    F_print_green "Start uninstall of ver. $1. Please wait..."
    systemctl stop srv1cv8-$1@default.service 2> >( logger -t "script-err")
    systemctl disable srv1cv8-$1@default.service 2> >( logger -t "script-err")
    systemctl stop ras-$1.service 2> >( logger -t "script-err")
    systemctl disable ras-$1.service 2> >( logger -t "script-err")
    /opt/1cv8/x86_64/$1/uninstaller-full --mode unattended
    F_print_green "$1 was uninstalled."
}

F_install() {
    F_print_green "Installing $1. This will take 1-3 minutes, please wait..."
    chmod 700 ./setup-full-$1-x86_64.run
    ./setup-full-$1-x86_64.run --mode unattended --enable-components server,ws,server_admin
    systemctl link /opt/1cv8/x86_64/$1/srv1cv8-$1@.service
    systemctl enable srv1cv8-$1@default.service
    systemctl start srv1cv8-$1@default.service
    systemctl status srv1cv8-$1@default.service
}

F_clean() {
    F_print_green "Cleanup"
    for FILE in $(ls | grep "^.*\.run$"); do
        read -e -p "Do you want to delete installer of $(./$FILE --version | cut -f2 -d' ') now? " -i "n"
        if [[ $REPLY =~ ^[Yy]$ ]]; then rm ./$FILE; fi
    done
}

F_exit() {
    case $1 in 
    0 ) F_print_green "All done."; exit 0;;
    1 ) F_print_green "ERROR: Installer not found!"; exit 1;;
    2 ) F_print_green "Cancel by user"; exit 1;;
    esac
}

F_print_green "Checking installer version"
if [[ -z "$(ls | grep "^.*\.run$")" ]]; then
    F_print_green "Installer not found."
    read -e -p "Do you want to download insfaller from sc ftp now? " -i "y"
    if [[ $REPLY =~ ^[Yy]$ ]]; then 
        read -e -p "Login: " -i "user" LOGIN
        read -e -p "Password: " PASS
        read -e -p "Ftp address: " -i "172.18.0.6" ADDR
        read -e -p "Version: " -i "8.3.21.1709" VER
        read -e -p "Check: " -i "ftp://$LOGIN:$PASS@$ADDR/share/Soft/Other/1C/Linux/Server/$VER/setup-full-$VER-x86_64.run" FTPFULL
        wget "$FTPFULL" -q --show-progress --progress=bar:force:noscroll
        chmod 700 ./"${FTPFULL##*/}"
    fi
fi
for FILE in $(ls | grep "^.*\.run$"); do
    FILEVER=(${FILEVER[*]} $(./$FILE --version | cut -f2 -d' '))
done
if [[ -z "$FILEVER" ]]; then F_exit 1; fi

F_print_green "Checking installed version"
if [[ -d "/opt/1cv8/x86_64/" ]]; then 
    CURVER=($(ls /opt/1cv8/x86_64/))
else
    CURVER=()
fi

if [[ ${#FILEVER[*]} == 1 && ${#CURVER[*]} == 1 && "$FILEVER" != "$CURVER" ]]; then
    F_print_green "Found a single installed version and a single installer. A simple upgrade is available."
    read -e -p "Do you want to upgrade from $CURVER to $FILEVER now? " -i "y"
    if [[ $REPLY =~ ^[Yy]$ ]]; then 
        F_uninstall "$CURVER"
        F_install "$FILEVER"
        F_clean
        F_exit 0
    fi
fi

if [[ ${#CURVER[*]} > 0 ]]; then 
    for I in $CURVER; do
        F_print_green "WARNING: Ver. $I is installed."
        read -e -p "Do you want to uninstall $I now? " -i "y"
        if [[ $REPLY =~ ^[Yy]$ ]]; then F_uninstall "$I"; fi
    done
fi

if [[ ${#FILEVER[*]} > 1 ]]; then
    F_print_green "More than one installers was found: ${FILEVER[*]}"
    while : ;do
        read -e -p "Please enter version to install now: " -i "$FILEVER" VER
        if [[ ! " ${FILEVER[*]} " =~ " $VER " ]]; then 
            F_print_green "ERROR: Wrong version!"
        else 
            break
        fi
    done
else
    VER=$FILEVER
fi

read -e -p "Ready for install $VER. Continue? " -i "y"
if [[ ! $REPLY =~ ^[Yy]$ ]]; then F_exit 2; fi
F_install $VER
F_clean

F_exit 0
