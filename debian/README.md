# Debian scripts

# 1C server
## after os install
windows:
```
type $env:USERPROFILE\.ssh\id_rsa.pub | ssh {USER@ADDR} "mkdir .ssh; cat >> .ssh/authorized_keys"
```
linux:
```
ssh-copy-id {USER@ADDR}
```
then on target:
```
sudo -i
bash <(wget -qO- "https://gitlab.com/psix048/scripts/-/raw/main/debian/postinstall.sh")

```


## install and tune PortgreSQL
```
bash <(wget -qO- "https://gitlab.com/psix048/scripts/-/raw/main/debian/postgresql.sh")
```


## install 1C server
```
bash <(wget -qO- "https://gitlab.com/psix048/scripts/-/raw/main/debian/server1c.sh")
```


## install 1C pusk
```
bash <(wget -qO- "https://gitlab.com/psix048/scripts/-/raw/main/debian/pusk.sh")
```


## install 1C hasp
```
bash <(wget -qO- "https://gitlab.com/psix048/scripts/-/raw/main/debian/hasp.sh")
```
