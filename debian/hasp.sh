#!/bin/bash

# copy stdout and stderror to log
exec 1> >(tee >(logger -t "script-out")) 2> >(tee >&2 >(logger -t "script-err"))

apt -y install libc6-i386 libc6-amd64-i386-cross
wget -P /tmp https://ftp.etersoft.ru/pub/Etersoft/HASP/7.90/x86_64/Debian/9/haspd_7.90-eter2debian_amd64.deb
apt -y install /tmp/haspd_7.90*.deb
wget -P /tmp https://ftp.etersoft.ru/pub/Etersoft/HASP/7.90/x86_64/Debian/9/haspd-modules_7.90-eter2debian_amd64.deb
apt -y install /tmp/haspd-modules_7.90*.deb
rm /tmp/haspd_7.90*.deb
rm /tmp/haspd-modules_7.90*.deb
systemctl restart haspd.service
systemctl status haspd.service

echo "All done"
exit 0