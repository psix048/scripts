#!/bin/bash

# copy stdout and stderror to log
exec 1> >(tee >(logger -t "script-out")) 2> >(tee >&2 >(logger -t "script-err"))

F_print_green() {
    echo ""
    echo -e "\e[0;32m    $1 \e[0m"
    echo ""
}

read -e -p "PUSK Version: " -i "057" PUSKVER
read -e -p "PUSK FullURL: " -i "https://cloud.it-expertise.ru/s/ite-pusk-distr/download/ite-pusk-$PUSKVER.tar.gz" PUSKURL
wget "$PUSKURL" -q --show-progress --progress=bar:force:noscroll
PUSKFILE=${PUSKURL##*/}

F_print_green "Installing JDK"
apt -y install openjdk-17-jdk-headless
java -version

F_print_green "Installing PUSK"
tar xf "$PUSKFILE"
pusk/ite-pusk-linux.sh install
systemctl start ite-pusk
systemctl status ite-pusk
rm "$PUSKFILE"

F_print_green "All done."
exit 0