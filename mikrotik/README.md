# Mikrotik scripts

# wg_sc
# 1. run on client, copy public key
:local script wg_sc_client.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]

## vpndns
:local script vpndns.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]

# 2. run on server, paste public key
:local script wg_sc_srv.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]


# personal script
:local script personal.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]


:local script wg_bakery_client.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]