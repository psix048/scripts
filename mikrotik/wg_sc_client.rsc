#!ros7

:log info "Start script wg_sc_client"

#-- function for read from user
:local readinput do={:return}

:local ClientName [([/system/identity/get]->"name")]
:put ("\r\nCurrent Identify and Client name = \"".$ClientName."\"")
:put "Do you want to change Identify? [y/N]"
:if (([ /terminal/inkey timeout=30 ] % 32) = 25) do={
    :put "Enter new client Identify"
    :set ClientName [$readinput]
    /system/identity/set name=$ClientName}

:put "\r\nEnter client network number"
:local ClientNum [$readinput]

:local WgName "wg_sc"
:local WgMark "mark:sc"
:local WgServer "sc.vpn48.ru"
:local ServerPort "42422"
:local ClientPort "4242"
:local RemoteWgAddr "172.18.0.1"
:local ScKey "rEhAAAsXvApQPSuvSoX+ZmqqWmN/bp5oAH2Yq6kb3Ho="
:local VpnList "VPN"
:local VpnRegExp "^(v|V)(p|P)(n|N)\$"

:local WgIp [("172.18.".$ClientNum.".1/16")]
:local WgNet [("172.18.".$ClientNum.".0/24")]

if ([len [/ip/dhcp-server/find]]!=1) do={:put "Error: Multiple DHCP servers:"; foreach i in=[/ip/dhcp-server/find] do={:put [/ip/dhcp-server/get $i name ]}; :put ("interface ".[/ip/dhcp-server/get 0 interface]." will be used")}
if ([len [/ip/address/find interface=[/ip/dhcp-server/get 0 interface]]]!=1) do={:put "Error: Multiple addresses on interface:"; foreach i in=[/ip/address/find interface=bridge_lan] do={:put [/ip/address/get $i address ]}}
:local LanIp [/ip/address/get [find interface=[/ip/dhcp-server/get 0 interface]] address]
:local LanNet [/ip/address/get [find interface=[/ip/dhcp-server/get 0 interface]] network]
:local LanMask [:pick $LanIp [:find $LanIp "/"] [:len $LanIp]]

#-- add static dns record
if ([/ip/dns/static/find name="mt.$ClientName"]) do={} else={/ip/dns/static/add name="mt.$ClientName" address=[:pick $LanIp 0 [:find $LanIp "/"]]}

#-- wg
#-- -- interface
/interface/wireguard/add name=$WgName listen-port=$ClientPort comment=$WgMark 
#-- -- peer
/interface/wireguard/peers/add interface=$WgName endpoint-address=$WgServer endpoint-port=$ServerPort \
    persistent-keepalive=30 public-key=$ScKey allowed-address=$RemoteWgAddr comment=$WgMark
#-- -- ip
/ip/address/add interface=$WgName address=$WgIp

#-- route (not needed if exist /16 mask)
#/ip/route/add dst-address=$RemoteWgAddr gateway=$WgName

#-- firewall
:log info "... firewall:"
#-- -- netmap
:log info "... netmap"
/ip/firewall/nat/add chain=dstnat in-interface=$WgName dst-address=$WgNet action=netmap to-addresses=($LanNet.$LanMask) comment="netmap sc"
#-- -- accept input udp for wg
:log info "... udp for wg" 
/ip/firewall/filter/add chain=input protocol=udp port=$ClientPort comment="wg" \
    place-before=[/ip/firewall/filter/find chain=input action=drop !connection-state]
#-- -- add wg to vpn interface list
:log info "... vpn list"
:if ([/interface/list/find name~$VpnRegExp]="") do={/interface/list/add name=$VpnList}
    :if ([/interface/list/find name=$VpnList]="") do={/interface/list/set [/interface/list/find name~$VpnRegExp] name=$VpnList}
        /interface/list/member/add list=$VpnList interface=$WgName
#-- -- accept input for vpn interface list
:if ([/ip/firewall/filter/find chain="input" in-interface-list=$VpnList action="accept"]="") do={
    /ip/firewall/filter/add chain="input" in-interface-list=$VpnList comment="VPN net" \
        place-before=[/ip/firewall/filter/find chain=input action=drop !connection-state]}

#-- netwath for fix frozen wg
#:log info "add netwatch"
#/tool netwatch add comment=$WgMark disabled=no host=$RemoteWgAddr http-codes="" interval=25s packet-count=5 packet-interval=20ms start-delay=10s thr-loss-percent=99% type=icmp \
#    down-script=":local MARK \"$WgMark\"\r\n:log warning \"\$[/interface/wireguard/get [find comment=\"\$MARK\"] name] is down now\"" \
#    up-script=":local MARK \"$WgMark\"\r\n:log warning \"\$[/interface/wireguard/get [find comment=\"\$MARK\"] name] is up now\"" \
#    test-script=":local MARK \"$WgMark\"\r\n:if (\$status = \"down\") do={\r\n:log warning \"\$[/interface/wireguard/get [find comment=\"\$MARK\"] name] still down\"\r\n\
#        /interface/wireguard/disable [find comment=\"\$MARK\"]\r\n\
#        /interface/wireguard/peers/disable [find comment=\"\$MARK\"]\r\n\
#        :delay 11\r\n\
#        /interface/wireguard/enable [find comment=\"\$MARK\"]\r\n\
#        /interface/wireguard/peers/enable [find comment=\"\$MARK\"]\r\n}"

/tool/netwatch/add comment=$WgMark disabled=no type=icmp host=172.18.0.1 http-codes="" interval=5s packet-count=3 packet-interval=500ms start-delay=10s startup-delay=20s \
    up-script="\
        :local c \$comment\r\n\
        if ([/ip/firewall/layer7-protocol/find name=\"\$c\"]) do={\r\n\
            :log warning \"\\\"\$[/interface/wireguard/get [find comment=\$c] name]\\\" became up now\"\r\n\
            /ip/firewall/layer7-protocol/remove [find name=\"\$c\"]\r\n\
        } else={\r\n\
            :log warning \"\\\"\$[/interface/wireguard/get [find comment=\$c] name]\\\" is up\"\r\n\
        }" \
    down-script="\
        :local c \$comment\r\n\
        if ([/ip/firewall/layer7-protocol/find name=\"\$c\"]) do={\r\n\
            :local dt [/ip/firewall/layer7-protocol/get [find name=\$c] regexp ]\r\n\
            :log warning \"\\\"\$[/interface/wireguard/get [find comment=\$c] name]\\\" still down for \$([:timestamp] - [:totime \$dt])\"\r\n\
        } else={\r\n\
            :log warning \"\\\"\$[/interface/wireguard/get [find comment=\$c] name]\\\" became down now\"\r\n\
            /ip/firewall/layer7-protocol/add regexp=[:timestamp] name=\"\$c\"\r\n\
        }" \
    test-script="\
        :if (\$status = \"down\") do={\r\n\
            :local c \$comment\r\n\
            :local t [:pick \$since ([:find \$since \" \"]+1) [:len \$since]]\r\n\
            if ([/system/clock/get time] - \$t > 1m) do={\r\n\
                /tool/netwatch/set [find comment=\$c] start-delay=20s\r\n\
                :log warning \"\$[/interface/wireguard/get [find comment=\$c] name] down for 1 minute more and will be disabled now\"\r\n\
                /interface/wireguard/disable [find comment=\$c]\r\n\
                /interface/wireguard/peers/disable [find comment=\$c]\r\n\
                :delay 12\r\n\
                :log warning \"\$[/interface/wireguard/get [find comment=\$c] name] will be re-enabled now\"\r\n\
                /interface/wireguard/enable [find comment=\$c]\r\n\
                /interface/wireguard/peers/enable [find comment=\$c]\r\n\
                /tool/netwatch/set [find comment=\$c] start-delay=10s\r\n\
                :local dt [/ip/firewall/layer7-protocol/get [find name=\$c] regexp]\r\n\
                if ([:timestamp] - [:totime \$dt] > 1h) do={\r\n\
                    :log error \"\$[/interface/wireguard/get [find comment=\$c] name] down for 1 hour, try reboot\"\r\n\
                    /ip/firewall/layer7-protocol/remove [find name=\"\$c\"]\r\n\
                    /system/reboot\r\n\
                }\r\n\
            }\r\n\
        }"

#-- include vpndns
:local script vpndns.rsc; [:parse ([tool/fetch url="https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/$script" output=user as-value]->"data")]

#-- print public key
:put ("\r\nPublicKey: ".[/interface/wireguard/get $WgName public-key])
#-- -- and copy to telegram
:local Text [($ClientName." Wireguard PublicKey: ".[/interface/wireguard/get $WgName public-key])]
/tool/fetch url="https://api.telegram.org/bot885694624:AAFvta4c5DtJ35tTNUfgS8fHcF4bLGt7_6g/sendMessage?chat_id=67974001&text=$Text"
:log info "All done."
:put "All done."