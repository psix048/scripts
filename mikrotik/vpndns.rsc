#!ros7

:log info "Start script vpndns"

#-- dns suf to find and copy
#-- can take multiple: "suf1|suf2|suf3"
#-- keep empty to use Identify
local LocalDnsSuf ""

#-- wg mark to find primary ip
local WgMark "mark:sc"

#-- dns mark to find and remove
local DnsMark "mark:vpn"

#-- dns suf for new enries
local RemoteDnsSuf "vpn.sc"

:if ($LocalDnsSuf="") do={:set LocalDnsSuf [([/system/identity/get]->"name")]}

local WgIp [/ip/address/get [find interface=[/interface/wireguard/get [find comment=$WgMark] name]] address]
local Mask [([:toip [:pick $WgIp 0 [:find $WgIp "/"]]]&255.255.255.0)]

:foreach entry in=[/ip/dns/static/find comment=$DnsMark] do={
    /ip/dns/static/remove $entry
}

:foreach entry in=[/ip/dns/static/find name~"^.+\\.($LocalDnsSuf)\$"] do={
    /ip/dns/static/add comment=$DnsMark \
        name=([/ip/dns/static/get $entry name].".".$RemoteDnsSuf) \
        address=([/ip/dns/static/get $entry address]&0.0.0.255|$Mask)
}

:put [/ip/dns/static/print detail where comment="mark:vpn"]

:log info "All done."
:put "All done."