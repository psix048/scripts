#!ros7

:log info "Start script autoupdate"

:local updated false
/system/package/update/check-for-updates once
:delay 5s
:if ( [/system/package/update/get status] = "New version is available" ) do={
    :log warning "New packages found"
    /system/backup/save name="before_update"
    /system/package/update/install
    :set updated true
} else={ 
    :log info "Packages is already up to date"
}

:if ( [/system/routerboard/get current-firmware] != [/system/routerboard/get upgrade-firmware] ) do={
    :log warning "New firmware found"
    /system/routerboard/upgrade
    :delay 10s
    :set updated true
} else={
    log info "Firmware is already up to date"
}

:if ( $updated ) do={
    :log warning "Script will reboot routerboard now"
    /system/reboot

:log info "All done."