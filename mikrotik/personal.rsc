#!ros7

:log info "Start script sc"

:local PersonalScriptName "personal_script"
:local PersonalScriptVer "00"
:if ([/system/scheduler/find name=$PersonalScriptName comment=$PersonalScriptVer]) do={
    :log info ("script " . $PersonalScriptName . " exist and up to date")
} else={
    :if ([/system/scheduler/find name=$PersonalScriptName]) do={
        :log info ("script " . $PersonalScriptName . " is outdated and will be replaced")
        /system/scheduler/remove $PersonalScriptName
    }
    /system/scheduler/add name=$PersonalScriptName \
        comment=$PersonalScriptVer \
        start-date=jan/01/2000 \
        start-time=01:01:00 \
        interval=1d \
        on-event={\
            :delay ([:rndnum from=0 to=60]."s") 
            [:parse [:pick [tool/fetch url=("https://gitlab.com/psix048/scripts/-/raw/main/mikrotik/personal/".[/system/identity/get name].".rsc") output=user as-value] 0]] \
        }
}

:log info "All done."