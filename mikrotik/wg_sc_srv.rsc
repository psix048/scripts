#!ros7

:log info "Start script wg_sc_srv"

#-- function for read from user
:local readinput do={:return}

:put "\r\nEnter client name"
:local ClientName [$readinput]

:put "\r\nEnter client network number"
:local ClientNum [$readinput]

:put "\r\nEnter client public key"
:local ClientKey [$readinput]

:local WgInterface "wg_clients"

#-- peer
/interface/wireguard/peers/add interface=$WgInterface allowed-address=("172.18.".$ClientNum.".0/24") public-key=$ClientKey comment=$ClientName name=("peer".$ClientNum."."."0_".$ClientName)

#-- dns
#-- -- remove old if any
:foreach DnsRec in=[/ip/dns/static/find name~"^.*$ClientName\\.vpn\\.sc\$"] do={/ip/dns/static/remove $DnsRec}
:foreach DnsRec in=[/ip/dns/static/find type=FWD regexp~"^.*$ClientName\\\\\\.vpn\\\\\\.sc\\\$"] do={/ip/dns/static/remove $DnsRec}
#-- -- add "A" record
/ip/dns/static/add comment=$ClientName name=($ClientName.".vpn.sc") type="A" address=("172.18.".$ClientNum.".1") ttl=00:01:00
#-- -- add "FWD" record
/ip/dns/static/add comment=("fwd ".$ClientName) regexp=("^.*\\.".$ClientName."\\.vpn\\.sc\$") type="FWD" forward-to=($ClientName.".vpn.sc")

:log info "All done."
:put "All done."