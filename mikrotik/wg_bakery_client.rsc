#!ros7

:log info "Start script wg_bakery_client"

#-- function for read from user
:local readinput do={:return}

:local ClientName [([/system/identity/get]->"name")]
:put ("\r\nCurrent Identify and Client name = \"".$ClientName."\"")
:put "Do you want to change Identify? [y/N]"
:if (([ /terminal/inkey timeout=30 ] % 32) = 25) do={
    :put "Enter new client Identify"
    :set ClientName [$readinput]
    /system/identity/set name=$ClientName}

:put "\r\nEnter client network number"
:local ClientNum [$readinput]

:local WgName "wg_bakery"
:local WgMark "mark:bakery"
:local WgServer "bakery1.vpn48.ru"
:local ServerPort "4241"
:local ClientPort "4241"
:local RemoteWgAddr "10.2.1.1"
:local AllowedIp "192.168.7.10"
:local BakeryKey "emiowGO06WOt69Jjmk0yS82lOhr6oB2MYR+m9fpX4h4="
:local VpnList "VPN"
:local VpnRegExp "^(v|V)(p|P)(n|N)\$"
:local WgIp [("10.2.1.".$ClientNum)]

#-- wg
#-- -- interface
/interface/wireguard/add name="$WgName" comment="$WgMark" listen-port="$ClientPort"
#-- -- peer
/interface/wireguard/peers/add interface="$WgName" endpoint-address="$WgServer" endpoint-port="$ServerPort" \
    persistent-keepalive=30 public-key="$BakeryKey" allowed-address="$RemoteWgAddr,$AllowedIp"
#-- -- ip
/ip/address/add interface="$WgName" address=("$WgIp"."/24")

#-- route
/ip/route/remove [find dst-address="$AllowedIp"]
/ip/route/add dst-address="$AllowedIp" gateway="$WgName"

#-- firewall
#-- -- accept input udp for wg
#/ip/firewall/filter/add chain=input protocol=udp port=$ClientPort comment="wg" \
#    place-before=[/ip/firewall/filter/find chain=input action=drop !connection-state in-interface-list]
#-- -- add wg to vpn interface list
#:if ([/interface/list/find name~$VpnRegExp]="") do={/interface/list/add name=$VpnList}
#    :if ([/interface/list/find name=$VpnList]="") do={/interface/list/set [/interface/list/find name~$VpnRegExp] name=$VpnList}
#        /interface/list/member/add list=$VpnList interface=$WgName
#-- -- accept input for vpn interface list
#:if ([/ip/firewall/filter/find chain="input" in-interface-list=$VpnList action="accept"]="") do={
#    /ip/firewall/filter/add chain="input" in-interface-list=$VpnList comment="VPN net" \
#        place-before=[/ip/firewall/filter/find chain=input action=drop !connection-state in-interface-list]}

#-- dns
/ip/dns/static/remove [find name="iikoserver.bakery1"]
/ip/dns/static/add name="iikoserver.bakery1" address="$AllowedIp"

#-- netwath for fix frozen wg
/tool netwatch add comment="$WgMark" thr-loss-percent=100% type=icmp host=$RemoteWgAddr \
    packet-count=5 interval=20s packet-interval=20ms start-delay=20s \
    up-script=(":log warning \"".$WgName." is up now\"") \
    down-script=(":log warning \"".$WgName." is down now\"") \
    test-script=(\
        ":if (\$status = \"down\") do={\r\n".\
        "    :log warning \"restarting ".$WgName."\"\r\n".\
        "    /interface/wireguard/disable [/interface/wireguard/find comment=\"".$WgMark."\"]\r\n".\
        "    :delay 2\r\n".\
        "    /interface/wireguard/enable [/interface/wireguard/find comment=\"".$WgMark."\"] \r\n".\
        "}")

#-- print public key
:put ("\r\nPublicKey: ".[/interface/wireguard/get $WgName public-key])
#-- -- and copy to telegram
:local Text [($ClientName." Wireguard PublicKey: ".[/interface/wireguard/get $WgName public-key])]
/tool/fetch url="https://api.telegram.org/bot885694624:AAFvta4c5DtJ35tTNUfgS8fHcF4bLGt7_6g/sendMessage?chat_id=67974001&text=$ClientName Wireguard PublicKey: $[/interface/wireguard/get $WgName public-key]"
:log info "All done."
:put "All done."